import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AjouterProduitLaststepPageRoutingModule } from './ajouter-produit-laststep-routing.module';

import { AjouterProduitLaststepPage } from './ajouter-produit-laststep.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AjouterProduitLaststepPageRoutingModule
  ],
  declarations: [AjouterProduitLaststepPage]
})
export class AjouterProduitLaststepPageModule {}
