import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AjouterProduitLaststepPage } from './ajouter-produit-laststep.page';

const routes: Routes = [
  {
    path: '',
    component: AjouterProduitLaststepPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AjouterProduitLaststepPageRoutingModule {}
