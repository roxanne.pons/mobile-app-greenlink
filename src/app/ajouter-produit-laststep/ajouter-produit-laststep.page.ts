import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/product/api.service";
import { ApiService as ApiServiceCategory } from "../services/category/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common";
import { PhotoService, UserPhoto } from '../services/photo.service';
import { ActionSheetController } from '@ionic/angular'; //to delete pics in photo library


@Component({
  selector: 'app-ajouter-produit-laststep',
  templateUrl: './ajouter-produit-laststep.page.html',
  styleUrls: ['./ajouter-produit-laststep.page.scss'],
})
export class AjouterProduitLaststepPage implements OnInit {
  public farmId: number;
  public name: string;
  public unity: string;
  public price: string;
  public stock: string;
  public description: string;
  public reduction: string;
  public images: any;
  public recipe: any;
  public categoryId: number;
  public categoryFamily: any;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private apiCategory: ApiServiceCategory,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location,
    public photoService: PhotoService,
    public actionSheetController: ActionSheetController


  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',
      duration: 500,
    });
    await loading.present();
  }


  addPhotoToGallery() {
    let result: any = this.photoService.addNewToGallery();
    console.log(result)
  }


  ngOnInit() {

    //this.photoService.loadSaved();

    this.storage.get('userId').then((farmId) => {
      this.farmId = farmId;

    });

    this.storage.get('category4').then((categoryId) => {
      this.categoryId = categoryId;
    });

    this.storage.get('categoryFamily').then((categoryFamily) => {
      this.categoryFamily = categoryFamily;
    });


  }

  add() {

    this.presentLoading();

    this.api.add1(
      {
        farmId: this.farmId,
        unity: this.unity,
        name: this.name,
        stock: this.stock,
        price: this.price,
        description: this.description,
        reduction: this.reduction,
        category: this.categoryId,
        photos: JSON.stringify(this.photoService.photos)
      });

    /*
    this.api.add(
      {
        farmId: this.farmId,
        unity: this.unity,
        name: this.name,
        stock: this.stock,
        price: this.price,
        description: this.description,
        reduction: this.reduction,
        category: this.categoryId,
        photos : JSON.stringify(this.photoService.photos)
      }).subscribe((data: { status: any, result: any }) => {

        if (data.status == 200) {

          this.navController.navigateRoot('/myproducts');

        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });

     */

  }
  public async showActionSheet(photo: UserPhoto, position: number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
        }
      }]
    });
    await actionSheet.present();
  }


}
