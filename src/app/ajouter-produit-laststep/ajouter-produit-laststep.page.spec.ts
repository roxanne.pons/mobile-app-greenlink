import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AjouterProduitLaststepPage } from './ajouter-produit-laststep.page';

describe('AjouterProduitLaststepPage', () => {
  let component: AjouterProduitLaststepPage;
  let fixture: ComponentFixture<AjouterProduitLaststepPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterProduitLaststepPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AjouterProduitLaststepPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
