import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/farm/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow



@Component({
  selector: 'app-membershiptype',
  templateUrl: './membershiptype.page.html',
  styleUrls: ['./membershiptype.page.scss'],
})
export class MembershiptypePage implements OnInit {

  //----- radio bug fixign

  defaultSelectedRadio = null;
  //Get value on ionChange on IonRadioGroup
  selectedRadioGroup: any;
  //Get value on ionSelect on IonRadio item
  selectedRadioItem: any;
  radio_list = [
    {
      id: '1',
      name: 'radio_list',
      value: '1',
      text: 'Classic',
      disabled: false,
      checked: true,
      color: 'primary',
      price: 'Gratuit',
      chip1: 'Ventre direct C2C',
      chip2: 'Limite de 5 produits par mois',

    }, {
      id: '2',
      name: 'radio_list',
      value: '2',
      text: 'Premium',
      disabled: false,
      checked: false,
      color: 'primary',
      price: '50 CHF / mois',
      chip1: 'Ventre direct C2C',
      chip2: 'Limite de 15 produits par mois',

    }, {
      id: '3',
      name: 'radio_list',
      value: '3',
      text: 'Gold',
      disabled: false,
      checked: false,
      color: 'primary',
      price: '150 CHF / mois',
      chip1: 'Vente direct C2C & B2B',
      chip2: 'Aucune limite de produits',
      chip3: 'Assistance gratuite',
      chip4: 'Mise en avant sur la plateforme',

    }
  ];

  radioGroupChange(event) {
    console.log("radioGroupChange", event.detail);
    this.selectedRadioGroup = event.detail;
    this.memberShip = event.detail.value;
  }

  radioFocus() {
    console.log("radioFocus");
  }
  radioSelect(event) {
    console.log("radioSelect", event.detail);
    this.selectedRadioItem = event.detail;
    this.memberShip = event.detail;

  }
  radioBlur() {
    console.log("radioBlur");
  }

  // -------- end radio bug fixign


  public farmId: number;
  public memberShip: string = null;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location

  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',
      duration: 500,
    });
    await loading.present();
  }

  ngOnInit() {
    this.storage.get('userId').then((farmId) => {
      this.farmId = farmId;
    });
  }


  submit() {
    this.presentLoading();

    if (!this.memberShip) {
      return false;
    }


    this.api.setMemberShip({ farmId: this.farmId, memberShip: this.memberShip }).subscribe((data: { status: any, result: any }) => {

      if (data.status == 200) {

        this.navController.navigateRoot('/homepage-agriculteur');

      } else {
        alert("Une erreur est survenue !");
      }

    }, (error) => {
      alert("Une erreur est survenue !");
      console.log(error)
    });
  }


}
