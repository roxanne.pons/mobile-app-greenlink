import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MembershiptypePage } from './membershiptype.page';

const routes: Routes = [
  {
    path: '',
    component: MembershiptypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MembershiptypePageRoutingModule {}
