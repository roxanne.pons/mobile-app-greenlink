import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MembershiptypePageRoutingModule } from './membershiptype-routing.module';

import { MembershiptypePage } from './membershiptype.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MembershiptypePageRoutingModule
  ],
  declarations: [MembershiptypePage]
})
export class MembershiptypePageModule {}
