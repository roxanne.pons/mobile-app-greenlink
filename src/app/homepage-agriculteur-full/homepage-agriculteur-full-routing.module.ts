import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomepageAgriculteurFullPage } from './homepage-agriculteur-full.page';

const routes: Routes = [
  {
    path: '',
    component: HomepageAgriculteurFullPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomepageAgriculteurFullPageRoutingModule {}
