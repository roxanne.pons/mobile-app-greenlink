import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomepageAgriculteurFullPage } from './homepage-agriculteur-full.page';

describe('HomepageAgriculteurFullPage', () => {
  let component: HomepageAgriculteurFullPage;
  let fixture: ComponentFixture<HomepageAgriculteurFullPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageAgriculteurFullPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomepageAgriculteurFullPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
