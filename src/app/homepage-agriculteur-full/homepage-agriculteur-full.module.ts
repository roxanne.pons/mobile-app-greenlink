import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomepageAgriculteurFullPageRoutingModule } from './homepage-agriculteur-full-routing.module';

import { HomepageAgriculteurFullPage } from './homepage-agriculteur-full.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomepageAgriculteurFullPageRoutingModule
  ],
  declarations: [HomepageAgriculteurFullPage]
})
export class HomepageAgriculteurFullPageModule {}
