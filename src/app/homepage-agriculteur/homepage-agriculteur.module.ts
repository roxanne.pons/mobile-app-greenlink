import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomepageAgriculteurPageRoutingModule } from './homepage-agriculteur-routing.module';

import { HomepageAgriculteurPage } from './homepage-agriculteur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomepageAgriculteurPageRoutingModule
  ],
  declarations: [HomepageAgriculteurPage]
})
export class HomepageAgriculteurPageModule {}
