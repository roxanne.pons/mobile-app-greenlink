import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/product/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow

@Component({
  selector: 'app-homepage-agriculteur',
  templateUrl: './homepage-agriculteur.page.html',
  styleUrls: ['./homepage-agriculteur.page.scss'],
})
export class HomepageAgriculteurPage implements OnInit {

  public user: any = { firstName: null };
  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location

  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  ngOnInit() {

    this.storage.get('user').then((user) => {
      this.user = user;
    });
  }

}
