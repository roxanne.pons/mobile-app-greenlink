import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomepageAgriculteurPage } from './homepage-agriculteur.page';

const routes: Routes = [
  {
    path: '',
    component: HomepageAgriculteurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomepageAgriculteurPageRoutingModule {}
