import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/product/api.service";
import { NavController } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-myproducts',
  templateUrl: './myproducts.page.html',
  styleUrls: ['./myproducts.page.scss'],
})
export class MyproductsPage implements OnInit {
  data: any;

  public products: any;
  public farmId: number;
  public count: number;


  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController
  ) {
    this.storage.create();
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 500);
  }


  ngOnInit() {


    this.storage.get('userId').then((farmId) => {
      this.farmId = farmId;
      this.count = 0;


      this.api.getProductsByFarm(this.farmId).subscribe((data) => {

        if (data) {
          // récup liste des produits depuis l'api
          this.products = data;
          this.count = this.products.length;

          this.products.forEach((item, i) => {
            let photos = JSON.parse(item.photos);
            if(photos){
              item.photo = photos[0].data;
            }else{
              item.photo = 'assets/img/pommes.jpg';
            }
          });

        } else {
          console.log("Une erreur est survenue !");
        }

      }, (error) => {
        console.log(error)
      });


    });
  }

  getProductDetail(productId) {
    this.storage.set('productId', productId)

    this.navController.navigateRoot('/product');

  }

}
