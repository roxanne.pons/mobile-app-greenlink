import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/product/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow



@Component({
  selector: 'app-farm-detail',
  templateUrl: './farm-detail.page.html',
  styleUrls: ['./farm-detail.page.scss'],
})
export class FarmDetailPage implements OnInit {
  data: any;

  public products: any;
  public farmId: number;
  public count: number;


  displayedImage: string = 'assets/icon/like.svg';


  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location

  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }


  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 500);
  }


  ngOnInit() {
    this.storage.get('userId').then((farmId) => {
      this.farmId = farmId;
      this.count = 0;


      this.api.getProductsByFarm(this.farmId).subscribe((data) => {

        if (data) {
          // récup liste des produits depuis l'api
          this.products = data;
          this.count = this.products.length;

        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });


    });

  }

  displayImage(variant) {
    if (this.displayedImage == 'assets/icon/like.svg') {
      this.displayedImage = 'assets/icon/like-full.svg';
    }

    else {
      this.displayedImage = 'assets/icon/like.svg';
    }
  }

}
