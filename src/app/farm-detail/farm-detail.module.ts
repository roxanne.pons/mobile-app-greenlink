import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FarmDetailPageRoutingModule } from './farm-detail-routing.module';

import { FarmDetailPage } from './farm-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FarmDetailPageRoutingModule
  ],
  declarations: [FarmDetailPage]
})
export class FarmDetailPageModule {}
