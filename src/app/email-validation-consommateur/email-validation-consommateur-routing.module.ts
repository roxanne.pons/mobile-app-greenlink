import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmailValidationConsommateurPage } from './email-validation-consommateur.page';

const routes: Routes = [
  {
    path: '',
    component: EmailValidationConsommateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmailValidationConsommateurPageRoutingModule {}
