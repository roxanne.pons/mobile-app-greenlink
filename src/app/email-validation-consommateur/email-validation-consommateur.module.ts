import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmailValidationConsommateurPageRoutingModule } from './email-validation-consommateur-routing.module';

import { EmailValidationConsommateurPage } from './email-validation-consommateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmailValidationConsommateurPageRoutingModule
  ],
  declarations: [EmailValidationConsommateurPage]
})
export class EmailValidationConsommateurPageModule {}
