import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmailValidationConsommateurPage } from './email-validation-consommateur.page';

describe('EmailValidationConsommateurPage', () => {
  let component: EmailValidationConsommateurPage;
  let fixture: ComponentFixture<EmailValidationConsommateurPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailValidationConsommateurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmailValidationConsommateurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
