import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/buyer/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow



@Component({
  selector: 'app-email-validation-consommateur',
  templateUrl: './email-validation-consommateur.page.html',
  styleUrls: ['./email-validation-consommateur.page.scss'],
})
export class EmailValidationConsommateurPage implements OnInit {

  public userId: number;
  public code: string;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location

  ) {
    this.storage.create();
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',
      duration: 500,
    });
    await loading.present();
  }


  ngOnInit() {
    this.storage.get('userId').then((userId) => {
      this.userId = userId;


      this.api.emailValidationRequest({ id: this.userId }).subscribe((data: { status: any, result: any }) => {

        if (data.status == 200) {

        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });


    });

  }
  comfirmCode() {
    this.presentLoading();

    this.api.emailValidationComfirm({ id: this.userId, code: this.code }).subscribe((data: { status: any, result: any }) => {

      if (data.status == 200) {
        this.storage.set('buyer', data.result);
        this.navController.navigateRoot('/payment-method');

      } else {
        alert("Une erreur est survenue !");
      }

    }, (error) => {
      alert("Une erreur est survenue !");
      console.log(error)
    });
  }


}
