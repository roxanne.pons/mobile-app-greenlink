import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MarketplaceFarmsPageRoutingModule } from './marketplace-farms-routing.module';

import { MarketplaceFarmsPage } from './marketplace-farms.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MarketplaceFarmsPageRoutingModule
  ],
  declarations: [MarketplaceFarmsPage]
})
export class MarketplaceFarmsPageModule {}
