import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-marketplace-farms',
  templateUrl: './marketplace-farms.page.html',
  styleUrls: ['./marketplace-farms.page.scss'],
})
export class MarketplaceFarmsPage implements OnInit {
  PopUpDisplay: string = "none";

  constructor() { }

  ngOnInit() {
  }

  PopUp(variant) {
    this.PopUpDisplay = 'block';
  }
}
