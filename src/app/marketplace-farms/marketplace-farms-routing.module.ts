import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarketplaceFarmsPage } from './marketplace-farms.page';

const routes: Routes = [
  {
    path: '',
    component: MarketplaceFarmsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarketplaceFarmsPageRoutingModule {}
