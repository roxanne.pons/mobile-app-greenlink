import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscriptionConsommateurPageRoutingModule } from './inscription-consommateur-routing.module';

import { InscriptionConsommateurPage } from './inscription-consommateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InscriptionConsommateurPageRoutingModule
  ],
  declarations: [InscriptionConsommateurPage]
})
export class InscriptionConsommateurPageModule {}
