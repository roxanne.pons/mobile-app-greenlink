import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InscriptionConsommateurPage } from './inscription-consommateur.page';

const routes: Routes = [
  {
    path: '',
    component: InscriptionConsommateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscriptionConsommateurPageRoutingModule {}
