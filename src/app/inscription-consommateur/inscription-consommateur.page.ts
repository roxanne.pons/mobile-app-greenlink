import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

import { ApiService } from "../services/buyer/api.service";
import { NavController, Platform } from "@ionic/angular"; // NavCOntroller : naviguer, Platform : (ex : back button)
import { Storage } from "@ionic/storage";
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-inscription-consommateur',
  templateUrl: './inscription-consommateur.page.html',
  styleUrls: ['./inscription-consommateur.page.scss'],
})
export class InscriptionConsommateurPage implements OnInit {

  public firstName: string = "";
  public lastName: string = "";
  public email: string = "";
  public phone: string = "";
  public password: string = "";
  public passConfirm: string = "";

  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    private platform: Platform,
    public loadingController: LoadingController,
    private location: Location

  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',
      duration: 6000,
    });
    await loading.present();
  }


  ngOnInit() {

  }

  signupFirst() {
    this.presentLoading();

    // double sécurité, éviter des requétes de formulaire incomplet
    if (
      this.firstName.length < 1 ||
      this.lastName.length < 1 ||
      this.email.length < 1 ||
      this.phone.length < 1 ||
      this.passConfirm.length < 1 ||
      this.password.length < 1
    ) {
      alert("Toutes les informations sont obligatoire !");
      return;
    }

    let user = {
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      phone: this.phone,
      password: this.password,
      passConfirm: this.passConfirm
    };


    this.api.signup(user).subscribe((data: { status: any, result: any }) => {

      if (data.status == 200) {
        this.storage.set('userId', data.result.id);
        this.navController.navigateRoot('/sms-validation-consommateur');

      } else {
        alert(JSON.stringify(data))
        alert("Une erreur est survenue 1 !");
      }

    }, error => {
      alert(JSON.stringify(error))
      alert("Une erreur est survenue 2 !");
      console.log(error)
    });

  }


}
