import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from "@ionic/angular";
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-research-filters',
  templateUrl: './research-filters.page.html',
  styleUrls: ['./research-filters.page.scss'],
})
export class ResearchFiltersPage implements OnInit {

  constructor(
    private platform: Platform,
    private location: Location

  ) {
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }


  ngOnInit() {
  }

}
