import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResearchFiltersPageRoutingModule } from './research-filters-routing.module';

import { ResearchFiltersPage } from './research-filters.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResearchFiltersPageRoutingModule
  ],
  declarations: [ResearchFiltersPage]
})
export class ResearchFiltersPageModule {}
