import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResearchFiltersPage } from './research-filters.page';

const routes: Routes = [
  {
    path: '',
    component: ResearchFiltersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResearchFiltersPageRoutingModule {}
