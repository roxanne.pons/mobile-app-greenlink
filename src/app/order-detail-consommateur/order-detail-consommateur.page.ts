import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/order/api.service";
import { ApiServiceUtils } from "../services/utils/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow
import { ActionSheetController } from '@ionic/angular'; //modal box w/ options


@Component({
  selector: 'app-order-detail-consommateur',
  templateUrl: './order-detail-consommateur.page.html',
  styleUrls: ['./order-detail-consommateur.page.scss'],
})
export class OrderDetailConsommateurPage implements OnInit {

  imgName: string = 'checkround-inactive';


  public order: any = null;
  public card: any = [];
  public list = [];
  public count = 0;

  constructor(
    private platform: Platform,
    private location: Location,
    private storage: Storage,
    private api: ApiService,
    private apiUtils: ApiServiceUtils,
    private navController: NavController,
    public loadingController: LoadingController,
    public actionSheetController: ActionSheetController
  ) {
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

    this.storage.create();

  }


  arrowBack() {
    this.location.back();
  }

  changeIcon() {
    if (this.imgName == 'checkround-active') {
      this.imgName = 'checkround-inactive';
    } else
      this.imgName = 'checkround-active';
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Commande en préparation',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Commande en livraison',
        handler: () => {
          console.log('Play clicked');
        }
      }, {
        text: 'Commande terminée',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Supprimer la commande',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }


  ngOnInit() {
    this.storage.get('order-detail').then((order) => {
      this.order = order;
      console.log(this.order);
    });

    this.storage.get('card-detail').then((card) => {
      if (card != null && card.length > 0) {
        this.card = card;
        //[{product : {p1-f1}, quatity : X}, {p2-f2}, {p3-f2}] -> tous les produts/objects au meme level
        this.card.forEach((item) => {
          //if case vide, ca ajoute la case de la ferme en vide pour que ca existe deja
          if (item.product) {
            if (!this.list[item.product.farm.id]) {
              this.list[item.product.farm.id] = [];
            }

            this.count++;

            item.product.quantity = item.quantity;
            if (item.photos) {
              let photos = JSON.parse(item.photos);
              if (photos) {
                item.product.photo = photos[0].data;
              } else {
                item.product.photo = 'assets/img/pommes.jpg';
              }
            }


            this.list[item.product.farm.id].push(
              item.product
            );
          }

        })
        //pour eviter que ca boucle sur les id anterieurs et crée des empty
        let listTemp = [];
        this.list.forEach((item) => {
          if (item) {
            listTemp.push(item);
          }
        });

        this.list = listTemp;
        console.log(this.list)

      }
    });
  }

}
