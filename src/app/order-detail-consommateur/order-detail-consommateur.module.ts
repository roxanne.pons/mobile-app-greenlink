import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderDetailConsommateurPageRoutingModule } from './order-detail-consommateur-routing.module';

import { OrderDetailConsommateurPage } from './order-detail-consommateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderDetailConsommateurPageRoutingModule
  ],
  declarations: [OrderDetailConsommateurPage]
})
export class OrderDetailConsommateurPageModule {}
