import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderDetailConsommateurPage } from './order-detail-consommateur.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailConsommateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderDetailConsommateurPageRoutingModule {}
