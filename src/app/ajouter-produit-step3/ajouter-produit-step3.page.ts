import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, Platform } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/category/api.service";
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-ajouter-produit-step3',
  templateUrl: './ajouter-produit-step3.page.html',
  styleUrls: ['./ajouter-produit-step3.page.scss'],
})
export class AjouterProduitStep3Page implements OnInit {
  data: any;

  public categories: any = false;
  public categoryId: number;
  public categoryParent: string;
  public categoryFamily: any;
  public categoriesAutres: any;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    private platform: Platform,
    private location: Location

  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });
  }

  arrowBack() {
    this.location.back();
  }


  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 5000);
  }




  ngOnInit() {

    this.storage.get('autres').then((autres) => {
      this.categoriesAutres = autres;
    });

    this.storage.get('categoryFamily').then((categoryFamily) => {
      this.categoryFamily = categoryFamily;
    });

    this.storage.get('category2').then((categoryId) => {
      this.categoryId = categoryId;



      this.api.getCategorie(
        this.categoryId
      ).subscribe((data: { name: null }) => {
        if (data.name) {
          this.categoryParent = data.name;
        } else {
          console.log(data)
          //alert("Une erreur est survenue !");
        }

      }, (error) => {
        //alert("Une erreur est survenue !");
        console.log(error)
      });


      this.api.getCategories(
        this.categoryId
      ).subscribe((data: { status: any, result: any }) => {
        if (data.status == 200) {

          this.categories = data.result;
          //this.navController.navigateRoot('/homepage-agriculteur');

        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });
    });

  }

  setCategory(idCategory) {
    this.storage.set('category3', idCategory);
    this.api.getCategorieFamily(
      idCategory
    ).subscribe((data) => {

      this.storage.set('categoryFamily', data);

      if (this.categoriesAutres.indexOf(idCategory) >= 0) {
        this.navController.navigateRoot('/ajouter-produit-laststep');
      } else {
        this.navController.navigateRoot('/ajouter-produit-step4');
      }

    }, (error) => {
      //alert("Une erreur est survenue !");
      console.log(error)
    });


  }

}
