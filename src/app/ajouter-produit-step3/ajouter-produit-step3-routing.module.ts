import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AjouterProduitStep3Page } from './ajouter-produit-step3.page';

const routes: Routes = [
  {
    path: '',
    component: AjouterProduitStep3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AjouterProduitStep3PageRoutingModule {}
