import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AjouterProduitStep3PageRoutingModule } from './ajouter-produit-step3-routing.module';

import { AjouterProduitStep3Page } from './ajouter-produit-step3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AjouterProduitStep3PageRoutingModule
  ],
  declarations: [AjouterProduitStep3Page]
})
export class AjouterProduitStep3PageModule {}
