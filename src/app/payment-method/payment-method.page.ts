import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/order/api.service";
import { ApiServiceUtils } from "../services/utils/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.page.html',
  styleUrls: ['./payment-method.page.scss'],
})
export class PaymentMethodPage implements OnInit {
  buttonColor1: string = 'transparent';
  buttonColor2: string = 'transparent';
  buttonColor3: string = 'transparent';
  buttonColor4: string = 'transparent';

  public order: any = null;

  constructor(
    private platform: Platform,
    private location: Location,
    private storage: Storage,
    private api: ApiService,
    private apiUtils: ApiServiceUtils,
    private navController: NavController,
    public loadingController: LoadingController

  ) {
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

    this.storage.create();

  }

  arrowBack() {
    this.location.back();
  }

  ngOnInit() {
    this.storage.forEach((item, i) => {
      console.log(item + ' ' + i);
    })
    this.storage.get('order').then((order) => {
      this.order = order;
      console.log(this.order);
      this.navController.navigateRoot('/payment-method');
    });
  }

  changeColor1(variant) {
    this.buttonColor1 = '#F2FAF4';
    this.buttonColor2 = 'transparent';
    this.buttonColor3 = 'transparent';
    this.buttonColor4 = 'transparent';
  }
  changeColor2(variant) {
    this.buttonColor2 = '#F2FAF4';
    this.buttonColor1 = 'transparent';
    this.buttonColor3 = 'transparent';
    this.buttonColor4 = 'transparent';

  }
  changeColor3(variant) {
    this.buttonColor3 = '#F2FAF4';
    this.buttonColor1 = 'transparent';
    this.buttonColor2 = 'transparent';
    this.buttonColor4 = 'transparent';

  }
  changeColor4(variant) {
    this.buttonColor4 = '#F2FAF4';
    this.buttonColor1 = 'transparent';
    this.buttonColor2 = 'transparent';
    this.buttonColor3 = 'transparent';

  }

  pay() {
    console.log(this.order)
    this.api.pay(
      this.order.id).subscribe((data: { status: any, result: any }) => {

        if (data.status == 200) {
          //oublier en session
          this.storage.set('has_order', false);

          this.storage.get('card').then((card) => {
            this.storage.set('card-detail', card);
            // reset panier
            this.storage.remove('card');

          })
          this.storage.remove('order');
          this.storage.set('order-detail', data.result);
          this.navController.navigateRoot('/successful-order');

        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });

  }

}
