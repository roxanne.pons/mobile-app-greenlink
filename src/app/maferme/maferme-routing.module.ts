import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MafermePage } from './maferme.page';

const routes: Routes = [
  {
    path: '',
    component: MafermePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MafermePageRoutingModule { }
