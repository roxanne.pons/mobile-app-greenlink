import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MafermePageRoutingModule } from './maferme-routing.module';

import { MafermePage } from './maferme.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MafermePageRoutingModule
  ],
  declarations: [MafermePage]
})
export class MafermePageModule { }
