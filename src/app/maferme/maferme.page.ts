import { Component, OnInit } from '@angular/core';
import { ApiService } from "../services/farm/api.service";
import { NavController, Platform } from "@ionic/angular";
import { Storage } from "@ionic/storage"; // NavCOntroller : naviguer, Platform : (ex : back button)
import { Location } from "@angular/common"; // back arrow
import { PhotoService, UserPhoto } from '../services/photo.service';
import { ActionSheetController } from '@ionic/angular'; //to delete pics in photo library



@Component({
  selector: 'app-maferme',
  templateUrl: './maferme.page.html',
  styleUrls: ['./maferme.page.scss'],
})
export class MafermePage implements OnInit {

  public farmId: number;
  public name: string = "";
  public typeOperation: number;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    private platform: Platform,
    private location: Location,
    public photoService: PhotoService,
    public actionSheetController: ActionSheetController

  ) {
    this.storage.create();
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  addPhotoToGallery() {
    this.photoService.addNewToGallery();
  }


  arrowBack() {
    this.location.back();
  }



  async ngOnInit() {

    await this.photoService.loadSaved();


    this.storage.get('userId').then((farmId) => {
      console.log(farmId)
      this.farmId = farmId;
    });
  }


  signupSecond() {
    // double sécurité, éviter des requétes de formulaire incomplet
    if (
      this.name.length < 1 ||
      this.typeOperation < 1
    ) {
      alert("Toutes les informations sont obligatoires !");
      return;
    }

    let farm = {
      farmId: this.farmId,
      name: this.name,
      typeOperation: this.typeOperation,
    };



    this.api.signupSecond(farm).subscribe((data: { status: any, result: any }) => {

      if (data.status == 200) {
        this.navController.navigateRoot('/membershiptype');

      } else {
        alert("Une erreur est survenue !");
      }

    }, (error) => {
      alert("Une erreur est survenue !");
      console.log(error)
    });

  }

  public async showActionSheet(photo: UserPhoto, position: number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
        }
      }]
    });
    await actionSheet.present();
  }


}
