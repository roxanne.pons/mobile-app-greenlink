import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilConsommateurPageRoutingModule } from './profil-consommateur-routing.module';

import { ProfilConsommateurPage } from './profil-consommateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilConsommateurPageRoutingModule
  ],
  declarations: [ProfilConsommateurPage]
})
export class ProfilConsommateurPageModule {}
