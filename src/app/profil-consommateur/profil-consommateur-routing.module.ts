import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilConsommateurPage } from './profil-consommateur.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilConsommateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilConsommateurPageRoutingModule {}
