import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomepageConsommateurPage } from './homepage-consommateur.page';

const routes: Routes = [
  {
    path: '',
    component: HomepageConsommateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomepageConsommateurPageRoutingModule {}
