import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomepageConsommateurPageRoutingModule } from './homepage-consommateur-routing.module';

import { HomepageConsommateurPage } from './homepage-consommateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomepageConsommateurPageRoutingModule
  ],
  declarations: [HomepageConsommateurPage]
})
export class HomepageConsommateurPageModule {}
