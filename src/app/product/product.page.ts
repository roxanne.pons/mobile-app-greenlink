import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/product/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  public product: any = {
    id: null,
    name: null,
    price: null,
    unity: null,
    description: null,
    stock: null,
    reduction: null
  };

  public productId: number;
  public card: any = [];


  constructor(
    public toastController: ToastController,
    private storage: Storage,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location,
    private api: ApiService,
  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }


  ngOnInit() {
    this.storage.get('card').then((card) => {
      if (card != null && card.length > 0) {
        this.card = card;
      }
    });

    this.storage.get('productId').then((productId) => {
      this.productId = productId;

      this.api.getProductById(this.productId).subscribe((data) => {

        if (data) {
          // récup liste des produits depuis l'api
          this.product = data;
          let photos = JSON.parse(this.product.photos);
          if (photos) {
            this.product.photo = photos[0].data;
          } else {
            this.product.photo = 'assets/img/pommes.jpg';
          }
          this.product.photos = photos;


        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });


    });

  }

  update() {
    this.api.update(this.product).subscribe((data: any) => {

      if (data.result.id) {

        this.navController.navigateRoot('/myproducts');

      } else {
        alert("Une erreur est survenue !");
      }

    }, (error) => {
      alert("Une erreur est survenue !");
      console.log(error)
    });
  }

}
