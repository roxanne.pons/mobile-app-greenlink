import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  // pour accéder aux childs: categoryParents = false
  /*
  (null, true) 1 niveau
  (X, true) 1 niveau
  (X) childs
  (X, false) childs
*/
  getCategories(categoryId, categoryParents = false) {
    return this.http.get(this.apiUrl + 'product/categories/get?categoryId=' + categoryId + '&categoryParents=' + categoryParents);
  }

  getCategorie(categoryId) {
    return this.http.get(this.apiUrl + 'product/categorie?categoryId=' + categoryId);
  }

  getCategorieFamily(categoryId) {
    return this.http.get(this.apiUrl + 'product/categorie/family?categoryId=' + categoryId);
  }

}
