import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiServiceUtils {

  public buyer_connect: boolean = false;
  public has_order: boolean = false;

  constructor() { }

}
