import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  signup(user) {

    return this.http.get(this.apiUrl + 'buyer/signup?firstName=' + user.firstName + '&lastName=' + user.lastName + '&phone=' + user.phone + '&passwordConfirmation=' + user.passConfirm + '&email=' + user.email + '&password=' + user.password);
  }


  phoneValidationRequest(user) {
    return this.http.get(this.apiUrl + 'buyer/confirm/phone/request?idBuyer=' + user.id);
  }

  phoneValidationComfirm(user) {
    return this.http.get(this.apiUrl + 'buyer/confirm/phone/validation?idBuyer=' + user.id + '&code=' + user.code);
  }

  emailValidationRequest(user) {
    return this.http.get(this.apiUrl + 'buyer/confirm/email/request?idBuyer=' + user.id);
  }

  emailValidationComfirm(user) {
    return this.http.get(this.apiUrl + 'buyer/confirm/email/validation?idBuyer=' + user.id + '&code=' + user.code);
  }

  setMemberShip(user) {
    return this.http.get(this.apiUrl + 'farmer/set/memberShipType?idFarm=' + user.farmId + '&memberShipId=' + user.memberShip);
  }

  login(user) {
    return this.http.get(this.apiUrl + 'buyer/login?email=' + user.email + '&password=' + user.password);
  }



}
