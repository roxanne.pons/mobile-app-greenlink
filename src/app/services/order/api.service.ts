import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Storage } from "@ionic/storage";
import { NavController } from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient, private storage: Storage, private navController: NavController) { }


  add(card, buyerId) {
    card.push(buyerId);
    let postData = {
      "card": card
    }
    this.http.post(this.apiUrl + 'order/add', postData)
      .subscribe((data: any) => {
        console.log(data.result.order);
        this.storage.set('order', data.result.order);
        this.navController.navigateRoot('/payment-method');

      }, error => {
        console.log(error);
        alert('error add order')
        return false;
      });

  }

  getDetailByFarm(farmId) {
    return this.http.get(this.apiUrl + 'order/detail/farm?farmId=' + farmId);
  }

  getOrder(id) {
    return this.http.get(this.apiUrl + 'order/detail?orderId=' + id);
  }

  pay(orderId) {
    return this.http.get(this.apiUrl + 'order/pay?orderId=' + orderId);
  }


}
