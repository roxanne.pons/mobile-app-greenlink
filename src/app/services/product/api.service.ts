import { Injectable } from '@angular/core';
import { LoadingController, NavController } from "@ionic/angular";
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient, private navController: NavController) { }


  add1(product) {
    if (!product.reduction) {
      product.reduction = 0;
    }

    if (!product.photos) {
      product.photos = [];
    }

    let formData = {
      name: product.name,
      unity: product.unity,
      price: product.price,
      stock: product.stock,
      description: product.description,
      reduction: product.reduction,
      farm: product.farmId,
      category: product.category,
      photos: product.photos
    };

    this.http.post(this.apiUrl + 'product/add', formData)
      .subscribe((data: any) => {
        if (data.status == 200) {
          this.navController.navigateRoot('/myproducts');
        }

      }, error => {
        console.log(error);
        alert('error add product')
        return false;
      });

  }

  update(product) {

    return this.http.get(this.apiUrl + 'product/update?name=' + product.name + '&unity=' + product.unity + '&price=' + product.price + '&stock=' + product.stock + '&description=' + product.description + '&reduction=' + product.reduction + '&id=' + product.id);
  }


  getProductsByFarm(farmId) {
    return this.http.get(this.apiUrl + 'product/by/farm?farmId=' + farmId);
  }

  getProductById(productId) {
    return this.http.get(this.apiUrl + 'product/get?id=' + productId);
  }
}
