import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  signupFirst(user) {
    return this.http.get(this.apiUrl + 'farmer/signup/first?firstName=' + user.firstName + '&lastName=' + user.lastName + '&phone=' + user.phone + '&passwordConfirmation=' + user.passConfirm + '&email=' + user.email + '&password=' + user.password);
  }

  signupSecond(user) {
    return this.http.get(this.apiUrl + 'farmer/signup/second?idFarm=' + user.farmId + '&name=' + user.name + '&address=' + user.address + '&city=' + user.city + '&postalCode=' + user.postalCode + '&country=' + user.country + '&typeOperationId=' + user.typeOperation);
  }

  getTypeOperations() {
    return this.http.get(this.apiUrl + 'typeOperation/list');
  }

  phoneValidationRequest(user) {
    return this.http.get(this.apiUrl + 'farmer/confirm/phone/request?idFarm=' + user.farmId);
  }

  phoneValidationComfirm(user) {
    return this.http.get(this.apiUrl + 'farmer/confirm/phone/validation?idFarm=' + user.farmId + '&code=' + user.code);
  }

  emailValidationRequest(user) {
    return this.http.get(this.apiUrl + 'farmer/confirm/email/request?idFarm=' + user.farmId);
  }

  emailValidationComfirm(user) {
    return this.http.get(this.apiUrl + 'farmer/confirm/email/validation?idFarm=' + user.farmId + '&code=' + user.code);
  }

  setMemberShip(user) {
    return this.http.get(this.apiUrl + 'farmer/set/memberShipType?idFarm=' + user.farmId + '&memberShipId=' + user.memberShip);
  }

  login(user) {
    return this.http.get(this.apiUrl + 'farmer/login?email=' + user.email + '&password=' + user.password);
  }


}
