import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AjouterProduitStep4Page } from './ajouter-produit-step4.page';

const routes: Routes = [
  {
    path: '',
    component: AjouterProduitStep4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AjouterProduitStep4PageRoutingModule {}
