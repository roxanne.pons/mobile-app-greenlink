import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AjouterProduitStep4PageRoutingModule } from './ajouter-produit-step4-routing.module';

import { AjouterProduitStep4Page } from './ajouter-produit-step4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AjouterProduitStep4PageRoutingModule
  ],
  declarations: [AjouterProduitStep4Page]
})
export class AjouterProduitStep4PageModule {}
