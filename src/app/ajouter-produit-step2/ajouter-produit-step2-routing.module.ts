import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AjouterProduitStep2Page } from './ajouter-produit-step2.page';

const routes: Routes = [
  {
    path: '',
    component: AjouterProduitStep2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AjouterProduitStep2PageRoutingModule {}
