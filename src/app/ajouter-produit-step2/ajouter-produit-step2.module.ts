import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AjouterProduitStep2PageRoutingModule } from './ajouter-produit-step2-routing.module';

import { AjouterProduitStep2Page } from './ajouter-produit-step2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AjouterProduitStep2PageRoutingModule
  ],
  declarations: [AjouterProduitStep2Page]
})
export class AjouterProduitStep2PageModule {}
