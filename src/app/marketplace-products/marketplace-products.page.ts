
import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/product/api.service";
import { NavController } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-marketplace-products',
  templateUrl: './marketplace-products.page.html',
  styleUrls: ['./marketplace-products.page.scss'],
})
export class MarketplaceProductsPage implements OnInit {
  data: any;

  public products: any;
  public farmId: number;
  public count: number;
  public card: any = [];


  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController
  ) {
    this.storage.create();
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 500);
  }

  ngOnInit() {
    this.storage.get('card').then((card) => {
      if (card != null && card.length > 0) {
        this.card = card;
        console.log(this.card)
      }
    });

    this.count = 0;

    //argument null: apiPlatform comprend que ca veut dire tous les produits
    this.api.getProductsByFarm(null).subscribe((data) => {

      if (data) {
        // récup liste des produits depuis l'api
        this.products = data;
        this.count = this.products.length;
        this.products.forEach((item, i) => {
          let photos = JSON.parse(item.photos);
          if (photos) {
            item.photo = photos[0].data;
          } else {
            item.photo = 'assets/img/pommes.jpg';
          }
        });

      } else {
        alert("Une erreur est survenue !");
      }

    }, (error) => {
      alert("Une erreur est survenue !");
      console.log(error)
    });


  }

  getProduct(id) {

    this.storage.set('productId', id);
    this.navController.navigateRoot('/product-detail');
  }

}
