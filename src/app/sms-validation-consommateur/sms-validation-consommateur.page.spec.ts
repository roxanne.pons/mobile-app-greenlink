import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SmsValidationConsommateurPage } from './sms-validation-consommateur.page';

describe('SmsValidationConsommateurPage', () => {
  let component: SmsValidationConsommateurPage;
  let fixture: ComponentFixture<SmsValidationConsommateurPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsValidationConsommateurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SmsValidationConsommateurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
