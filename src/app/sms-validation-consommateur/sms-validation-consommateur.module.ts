import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SmsValidationConsommateurPageRoutingModule } from './sms-validation-consommateur-routing.module';

import { SmsValidationConsommateurPage } from './sms-validation-consommateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SmsValidationConsommateurPageRoutingModule
  ],
  declarations: [SmsValidationConsommateurPage]
})
export class SmsValidationConsommateurPageModule {}
