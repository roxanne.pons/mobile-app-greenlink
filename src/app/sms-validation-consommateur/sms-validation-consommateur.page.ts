import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

import { ApiService } from "../services/buyer/api.service";
import { NavController, Platform } from "@ionic/angular"; // NavCOntroller : naviguer, Platform : (ex : back button)
import { Storage } from "@ionic/storage";
import { Location } from "@angular/common"; // back arrow



@Component({
  selector: 'app-sms-validation-consommateur',
  templateUrl: './sms-validation-consommateur.page.html',
  styleUrls: ['./sms-validation-consommateur.page.scss'],
})
export class SmsValidationConsommateurPage implements OnInit {

  public userId: number;
  public code: string;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location

  ) {
    this.storage.create();
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',
      duration: 6000,
    });
    await loading.present();
  }

  ngOnInit() {

    this.storage.get('userId').then((userId) => {
      this.userId = userId;

      this.api.phoneValidationRequest({ id: this.userId }).subscribe((data: { status: any, result: any }) => {

        if (data.status == 200) {

        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });

    });

  }
  comfirmCode() {
    this.presentLoading();
    this.api.phoneValidationComfirm({ id: this.userId, code: this.code }).subscribe((data: { status: any, result: any }) => {

      if (data.status == 200) {

        this.navController.navigateRoot('/email-validation-consommateur');

      } else {
        alert("Une erreur est survenue !");
      }

    }, (error) => {
      alert("Une erreur est survenue !");
      console.log(error)
    });
  }


}
