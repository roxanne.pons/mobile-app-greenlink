import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SmsValidationConsommateurPage } from './sms-validation-consommateur.page';

const routes: Routes = [
  {
    path: '',
    component: SmsValidationConsommateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SmsValidationConsommateurPageRoutingModule {}
