import { Component, OnInit } from '@angular/core';
import { ActionSheetController, NavController, Platform } from "@ionic/angular";
import { Location } from "@angular/common";
import { ApiService } from "../services/order/api.service";
import { Storage } from "@ionic/storage";
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-myorders',
  templateUrl: './myorders.page.html',
  styleUrls: ['./myorders.page.scss'],
})
export class MyordersPage implements OnInit {
  data: any;

  bg: string = '#f3f2fa';
  bg2: string = 'white';
  borderBottom2: string = '0.5px solid #d3cfe8';
  borderBottom: string = 'none';

  public farmId: number;
  public found: boolean = false;
  public orders: any = [];


  constructor(
    private platform: Platform,
    private location: Location,
    public actionSheetController: ActionSheetController,
    private api: ApiService,
    private storage: Storage,
    private navController: NavController,
    public loadingController: LoadingController,


  ) {
    this.storage.create();

  }

  changeBg(variant) {
    this.bg2 = 'white';
    this.borderBottom = 'none';
    this.bg = '#f3f2fa';
    this.borderBottom2 = '0.5px solid #d3cfe8';
  }
  changeBg2(variant) {
    this.bg = 'white';
    this.borderBottom2 = 'none';
    this.bg2 = '#f3f2fa';
    this.borderBottom = '0.5px solid #d3cfe8';
  }


  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };

    }, 500);
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',
      duration: 500,
    });
    await loading.present();
  }


  ngOnInit() {

    this.storage.get('userId').then((farmId) => {
      this.presentLoading();
      this.farmId = farmId;
      this.api.getDetailByFarm(
        this.farmId
      ).subscribe((data: { status: any, result: any }) => {
        if (data.result.length > 0) {
          this.found = true;
          let orders = data.result;
          orders.forEach((item) => {
            let strToArr = item.ordre.split('/');
            let id = strToArr[strToArr.length - 1];
            if (!this.orders[id]) {
              this.orders[id] = [];
            }
            item.orderId = id;
            this.orders[id].push(item);
          });

          let listTemp = [];
          this.orders.forEach((item) => {
            if (item) {
              listTemp.push(item);
            }
          });
          this.orders = listTemp;

        } else {
          this.found = false
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });
    });

  }

  goToDetail(orderId) {
    this.storage.set('idOrder', orderId);
    this.navController.navigateRoot('/order-detail');
  }
}
