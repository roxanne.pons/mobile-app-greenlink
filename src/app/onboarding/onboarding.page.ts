import { Component, OnInit } from '@angular/core';

// import Swiper core and required modules
import SwiperCore, {
  Navigation,
  Pagination,
} from 'swiper/core';

SwiperCore.use([Navigation, Pagination]);

export class OnboardingPageModule { }


@Component({
  selector: 'app-onboarding',
  template: `
    <swiper
      [slidesPerView]="1"
      (swiper)="onSwiper($event)"
      (slideChange)="onSlideChange()"
      [navigation]="false"
      [pagination]="{ clickable: true }"
      [scrollbar]="{ draggable: true }"
    >


    <ng-template swiperSlide>
      <img src="assets/img/logo.svg" class="logo">
      <img src="assets/img/illu-homme.svg" class="illu">
      <div class="container">
        <h1>Cher producteur,</h1>
        <p class="big-text">Souhaites-tu vendre tes produits directement à des particuliers ?
          En choisissant tes prix et en définissant tes règles ? <br><br>
          Cette application est faite pour toi !</p>
      </div>

    </ng-template>

    <ng-template swiperSlide>
    <img src="assets/img/logo.svg" class="logo">
    <img src="assets/img/illu-femme.svg" class="illu">
    <div class="container">
      <h1>Cher consommateur,</h1>
      <p class="big-text">Aimerais-tu acheter des produits frais et locaux sans sortir de chez toi ?
        T’engager pour une consommation saine, éthique et écologique ?
        <br><br>
        Cette application est faite pour toi !</p>
    </div>
  </ng-template>
  <ng-template swiperSlide>
    <img src="assets/img/logo.svg" class="logo">
    <img src="assets/img/highfive.svg" class="illu clap">
    <div class="container">
      <h1>Greenlink,</h1>
      <p class="big-text">L’application qui permet de faire de la vente directe
        entre agriculteurs et consommateurs, en quelques clics !</p>
      <div class="buttons">
      <ion-button href="connexion-agriculteur" class="agri-btn">Vendre</ion-button>
      <ion-button href="connexion" class="consom-btn">Acheter</ion-button>
      </div>
    </div>
  </ng-template>
    </swiper>
  `,
  styleUrls: ['./onboarding.page.scss'],
})
export class OnboardingPage implements OnInit {

  constructor() { }

  ngOnInit() {

  }
  onSwiper(swiper) {
    console.log(swiper);

  }
  onSlideChange() {
    console.log('slide change');
  }


}

