import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/farm/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-email-validation',
  templateUrl: './email-validation.page.html',
  styleUrls: ['./email-validation.page.scss'],
})
export class EmailValidationPage implements OnInit {

  public farmId: number;
  public code: string;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location

  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });
  }

  arrowBack() {
    this.location.back();
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',
      duration: 700,
    });
    await loading.present();
  }

  ngOnInit() {
    this.storage.get('userId').then((farmId) => {
      this.farmId = farmId;


      this.api.emailValidationRequest({ farmId: this.farmId }).subscribe((data: { status: any, result: any }) => {

        if (data.status == 200) {

        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });


    });
  }

  comfirmCode() {
    this.presentLoading();

    this.api.emailValidationComfirm({ farmId: this.farmId, code: this.code }).subscribe((data: { status: any, result: any }) => {

      if (data.status == 200) {

        this.navController.navigateRoot('/maferme');

      } else {
        alert("Une erreur est survenue !");
      }

    }, (error) => {
      alert("Une erreur est survenue !");
      console.log(error)
    });
  }

}
