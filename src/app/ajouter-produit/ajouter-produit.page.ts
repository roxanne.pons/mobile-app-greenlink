import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, Platform } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/category/api.service";
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-ajouter-produit',
  templateUrl: './ajouter-produit.page.html',
  styleUrls: ['./ajouter-produit.page.scss'],
})
export class AjouterProduitPage implements OnInit {
  data: any;

  public categories: any = false;
  public categoriesAutres: any;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    private platform: Platform,
    private location: Location


  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });

  }

  arrowBack() {
    this.location.back();
  }


  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 5000);
  }

  ngOnInit() {
    this.categoriesAutres = [5, 11, 17, 22, 26, 30, 37, 45, 51, 59, 65, 72, 81, 87];
    this.storage.set('autres', this.categoriesAutres);

    this.api.getCategories(
      null, true
    ).subscribe((data: { status: any, result: any }) => {
      if (data.status == 200) {
        this.categories = data.result;
        //this.navController.navigateRoot('/homepage-agriculteur');

      } else {
        alert("Une erreur est survenue !");
      }

    }, (error) => {
      alert("Une erreur est survenue !");
      console.log(error)
    });

  }

  setCategory(idCategory) {
    this.storage.set('category', idCategory);

    if (this.categoriesAutres.indexOf(idCategory) >= 0) {
      this.navController.navigateRoot('/ajouter-produit-laststep');
      //else needed in typescrit so it doesnt jump to next step too fast
    } else {
      this.navController.navigateRoot('/ajouter-produit-step2');
    }
  }



}
