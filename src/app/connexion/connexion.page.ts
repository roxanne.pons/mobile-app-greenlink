import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, Platform } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/buyer/api.service";
import { ApiServiceUtils } from "../services/utils/api.service";
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {

  public email: string;
  public password: string;
  public has_order: boolean;

  constructor(
    private storage: Storage,
    private api: ApiService,
    private apiUtils: ApiServiceUtils,
    private navController: NavController,
    private platform: Platform,
    private location: Location

  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });
  }

  arrowBack() {
    this.location.back();
  }

  ngOnInit() {

    this.storage.get('has_order').then((has_order) => {
      if (has_order == true) this.has_order = true;
    });
  }

  login() {
    this.api.login(
      {
        email: this.email, password: this.password
      }).subscribe((data: { status: any, result: any }) => {

        if (data.status == 200) {
          this.storage.set('userId', data.result.id);
          this.storage.set('buyer', data.result);
          this.storage.set('buyer_connect', true);
          if (this.has_order) {
            this.navController.navigateRoot('/panier');
          } else {
            this.navController.navigateRoot('/homepage-consommateur');
          }


        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });

  }
}
