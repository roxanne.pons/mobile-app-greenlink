import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/product/api.service";
import { NavController, Platform } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';
import { Location } from "@angular/common"; // back arrow


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {

  public product: any;
  public productId: number;
  public card: any = [];
  //plusmoins quantity input
  private currentQuantity = 1;

  constructor(
    public toastController: ToastController,
    private storage: Storage,
    private api: ApiService,
    private navController: NavController,
    public loadingController: LoadingController,
    private platform: Platform,
    private location: Location


  ) {
    this.storage.create();

    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();
    });


  }

  arrowBack() {
    this.location.back();
  }

  increment() {
    this.currentQuantity++;
  }

  decrement() {
    this.currentQuantity--;
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',

      duration: 500,
    });
    await loading.present();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Le produit a bien été ajouté au panier !',
      duration: 1500,
      cssClass: "myCustomAlert",
      position: "top",
      translucent: true,
    });
    toast.present();

    this.card.push({
      product: this.product,
      quantity: this.currentQuantity
    });
    this.storage.set('card', this.card);
    this.navController.navigateRoot('/marketplace-products');

  }



  ngOnInit() {
    this.storage.get('card').then((card) => {
      if (card != null && card.length > 0) {
        this.card = card;
      }
    });

    this.presentLoading();

    this.storage.get('productId').then((productId) => {
      this.productId = productId;

      this.api.getProductById(this.productId).subscribe((data) => {

        if (data) {
          // récup liste des produits depuis l'api
          this.product = data;
          let photos = JSON.parse(this.product.photos);
          if (photos) {
            this.product.photo = photos[0].data;
          } else {
            this.product.photo = 'assets/img/pommes.jpg';
          }
          this.product.photos = photos;

        } else {
          alert("Une erreur est survenue !");
        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });


    });

  }

}
