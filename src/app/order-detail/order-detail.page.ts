import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from "@ionic/angular";
import { Location } from "@angular/common"; // back arrow
import { ActionSheetController } from '@ionic/angular'; //modal box w/ options
import { ApiService } from "../services/order/api.service";
import { Storage } from "@ionic/storage";


@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.page.html',
  styleUrls: ['./order-detail.page.scss'],
})
export class OrderDetailPage implements OnInit {
  data: any;
  public count = 0;
  imgName: string = 'checkround-inactive';
  public orderId: number;
  public userId: number;
  public order: any = null;
  // statut
  text: string = 'En attente';
  statutColor: string = '#ffc409';


  constructor(
    private platform: Platform,
    private location: Location,
    public actionSheetController: ActionSheetController,
    private api: ApiService,
    private storage: Storage,
    private navController: NavController

  ) {
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.location.back();

    });

  }

  arrowBack() {
    this.location.back();
  }

  changeIcon() {
    if (this.imgName == 'checkround-active') {
      this.imgName = 'checkround-inactive';
    } else
      this.imgName = 'checkround-active';
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({

      buttons: [{
        text: 'Commande en préparation',
        handler: () => {
          this.text = 'En préparation';
          this.statutColor = '#63b133';
          console.log('en préparation clicked');
        }
      }, {
        text: 'Commande en livraison',
        handler: () => {
          this.text = 'En livraison';
          this.statutColor = '#2f97ed';

          console.log('Play clicked');
        }
      }, {
        text: 'Commande terminée',
        handler: () => {
          this.text = 'Terminée';
          this.statutColor = '#afafaf';

          console.log('Favorite clicked');
        }
      }, {
        text: 'Annuler la commande',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'fermer',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };

    }, 400);
  }


  ngOnInit() {
    this.storage.create();

    this.storage.get('userId').then((userId) => {
      this.userId = userId;
    });


    this.storage.get('idOrder').then((orderId) => {
      this.orderId = orderId;

      this.api.getOrder(
        this.orderId
      ).subscribe((data: any) => {
        if (data.result) {
          this.order = data.result;
          this.order.cards.forEach((item, i) => {
            let farmId = item.farm.split('/');
            if (this.userId != farmId[farmId.length - 1]) {
              this.order.cards[i] = null;
            } else {
              this.count++;
            }

            if (item.product.photos) {
              let photos = JSON.parse(item.product.photos);
              if (photos) {
                item.product.photo = photos[0].data;
              } else {
                item.product.photo = 'assets/img/pommes.jpg';
              }
            }
          });

          console.log(this.order)

        }

      }, (error) => {
        alert("Une erreur est survenue !");
        console.log(error)
      });
    });



  }

}
