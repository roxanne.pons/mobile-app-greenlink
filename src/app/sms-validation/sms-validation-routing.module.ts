import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SmsValidationPage } from './sms-validation.page';

const routes: Routes = [
  {
    path: '',
    component: SmsValidationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SmsValidationPageRoutingModule {}
