import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SmsValidationPageRoutingModule } from './sms-validation-routing.module';

import { SmsValidationPage } from './sms-validation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SmsValidationPageRoutingModule
  ],
  declarations: [SmsValidationPage]
})
export class SmsValidationPageModule {}
