import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilAgriculteurPage } from './profil-agriculteur.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilAgriculteurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilAgriculteurPageRoutingModule {}
