import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilAgriculteurPageRoutingModule } from './profil-agriculteur-routing.module';

import { ProfilAgriculteurPage } from './profil-agriculteur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilAgriculteurPageRoutingModule
  ],
  declarations: [ProfilAgriculteurPage]
})
export class ProfilAgriculteurPageModule {}
