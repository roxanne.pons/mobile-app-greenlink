import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiService } from "../services/order/api.service";
import { ApiServiceUtils } from "../services/utils/api.service";
import { NavController } from "@ionic/angular";
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.page.html',
  styleUrls: ['./panier.page.scss'],
})
export class PanierPage implements OnInit {
  data: any;

  //variable partout on ou on a besoin du panier
  public card: any = [];
  public list = [];
  public has_order: boolean = false;
  public buyer_connect: boolean = false;
  public userId: number;
  //plus moins
  private currentQuantity = 0;


  constructor(
    private storage: Storage,
    private api: ApiService,
    private apiUtils: ApiServiceUtils,
    private navController: NavController,
    public loadingController: LoadingController
  ) {
    this.storage.create();
  }


  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };

    }, 500);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-loader-class',
      message: 'Un instant...',
      duration: 500,
    });
    await loading.present();
  }


  private increment() {
    this.currentQuantity++;
  }

  private decrement() {
    this.currentQuantity--;
  }

  ngOnInit() {
    this.presentLoading();

    this.storage.get('userId').then((userId) => {
      this.userId = userId;
    });

    this.storage.get('has_order').then((has_order) => {
      if (has_order == true) this.has_order = true;
    });

    this.storage.get('buyer_connect').then((buyer_connect) => {
      if (buyer_connect) this.buyer_connect = buyer_connect;
    });

    this.storage.get('card').then((card) => {
      console.log(card);
      if (card != null && card.length > 0) {
        this.card = card;
        //[{product : {p1-f1}, quatity : X}, {p2-f2}, {p3-f2}] -> tous les produts/objects au meme level
        this.card.forEach((item) => {
          //if case vide, ca ajoute la case de la ferme en vide pour que ca existe deja
          if (item.product) {
            if (!this.list[item.product.farm.id]) {
              this.list[item.product.farm.id] = [];
            }

            item.product.quantity = this.currentQuantity = item.quantity;

            this.list[item.product.farm.id].push(
              item.product
            );
          }

        })
        //pour eviter que ca boucle sur les id anterieurs et crée des empty
        let listTemp = [];
        this.list.forEach((item) => {
          if (item) {
            listTemp.push(item);
          }
        });

        this.list = listTemp;
        console.log(this.list)

      }
    });

  }

  command() {
    this.storage.set('has_order', true);
    if (this.buyer_connect) {

      this.api.add(this.list, this.userId);

    } else {
      this.navController.navigateRoot('/connexion');
    }

  }

}
