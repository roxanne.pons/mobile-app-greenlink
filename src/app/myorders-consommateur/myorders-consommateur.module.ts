import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyordersConsommateurPageRoutingModule } from './myorders-consommateur-routing.module';

import { MyordersConsommateurPage } from './myorders-consommateur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyordersConsommateurPageRoutingModule
  ],
  declarations: [MyordersConsommateurPage]
})
export class MyordersConsommateurPageModule {}
