import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyordersConsommateurPage } from './myorders-consommateur.page';

const routes: Routes = [
  {
    path: '',
    component: MyordersConsommateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyordersConsommateurPageRoutingModule {}
