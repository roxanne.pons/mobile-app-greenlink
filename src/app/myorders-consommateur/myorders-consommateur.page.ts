import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-myorders-consommateur',
  templateUrl: './myorders-consommateur.page.html',
  styleUrls: ['./myorders-consommateur.page.scss'],
})
export class MyordersConsommateurPage implements OnInit {
  bg: string = '#f3f2fa';
  bg2: string = 'white';
  borderBottom2: string = '0.5px solid #d3cfe8';
  borderBottom: string = 'none';

  constructor() { }

  changeBg(variant) {
    this.bg2 = 'white';
    this.borderBottom = 'none';
    this.bg = '#f3f2fa';
    this.borderBottom2 = '0.5px solid #d3cfe8';
  }
  changeBg2(variant) {
    this.bg = 'white';
    this.borderBottom2 = 'none';
    this.bg2 = '#f3f2fa';
    this.borderBottom = '0.5px solid #d3cfe8';
  }


  ngOnInit() {
  }

}
