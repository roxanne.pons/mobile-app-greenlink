import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuccessfulOrderPageRoutingModule } from './successful-order-routing.module';

import { SuccessfulOrderPage } from './successful-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuccessfulOrderPageRoutingModule
  ],
  declarations: [SuccessfulOrderPage]
})
export class SuccessfulOrderPageModule {}
