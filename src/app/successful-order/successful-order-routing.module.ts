import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuccessfulOrderPage } from './successful-order.page';

const routes: Routes = [
  {
    path: '',
    component: SuccessfulOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuccessfulOrderPageRoutingModule {}
