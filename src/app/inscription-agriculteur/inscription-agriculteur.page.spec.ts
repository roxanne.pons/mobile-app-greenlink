import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InscriptionAgriculteurPage } from './inscription-agriculteur.page';

describe('InscriptionAgriculteurPage', () => {
  let component: InscriptionAgriculteurPage;
  let fixture: ComponentFixture<InscriptionAgriculteurPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InscriptionAgriculteurPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InscriptionAgriculteurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
