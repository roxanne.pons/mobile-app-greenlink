import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscriptionAgriculteurPageRoutingModule } from './inscription-agriculteur-routing.module';

import { InscriptionAgriculteurPage } from './inscription-agriculteur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InscriptionAgriculteurPageRoutingModule
  ],
  declarations: [InscriptionAgriculteurPage]
})
export class InscriptionAgriculteurPageModule { }
