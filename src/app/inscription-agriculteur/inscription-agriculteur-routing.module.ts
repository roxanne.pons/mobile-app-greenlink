import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InscriptionAgriculteurPage } from './inscription-agriculteur.page';

const routes: Routes = [
  {
    path: '',
    component: InscriptionAgriculteurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscriptionAgriculteurPageRoutingModule { }
