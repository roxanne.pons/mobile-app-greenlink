import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./onboarding/onboarding.module').then(m => m.OnboardingPageModule)
  },
  {
    path: 'homepage-consommateur',
    loadChildren: () => import('./homepage-consommateur/homepage-consommateur.module').then(m => m.HomepageConsommateurPageModule)
  },
  {
    path: 'inscription-agriculteur',
    loadChildren: () => import('./inscription-agriculteur/inscription-agriculteur.module').then(m => m.InscriptionAgriculteurPageModule)
  },
  {
    path: 'sms-validation',
    loadChildren: () => import('./sms-validation/sms-validation.module').then(m => m.SmsValidationPageModule)
  },
  {
    path: 'maferme',
    loadChildren: () => import('./maferme/maferme.module').then(m => m.MafermePageModule)
  },
  {
    path: 'membershiptype',
    loadChildren: () => import('./membershiptype/membershiptype.module').then(m => m.MembershiptypePageModule)
  },
  {
    path: 'email-validation',
    loadChildren: () => import('./email-validation/email-validation.module').then(m => m.EmailValidationPageModule)
  },
  {
    path: 'homepage-agriculteur',
    loadChildren: () => import('./homepage-agriculteur/homepage-agriculteur.module').then(m => m.HomepageAgriculteurPageModule)
  },
  {
    path: 'ajouter-produit',
    loadChildren: () => import('./ajouter-produit/ajouter-produit.module').then(m => m.AjouterProduitPageModule)
  },
  {
    path: 'ajouter-produit-step2',
    loadChildren: () => import('./ajouter-produit-step2/ajouter-produit-step2.module').then(m => m.AjouterProduitStep2PageModule)
  },
  {
    path: 'ajouter-produit-laststep',
    loadChildren: () => import('./ajouter-produit-laststep/ajouter-produit-laststep.module').then(m => m.AjouterProduitLaststepPageModule)
  },
  {
    path: 'myproducts',
    loadChildren: () => import('./myproducts/myproducts.module').then(m => m.MyproductsPageModule)
  },
  {
    path: 'product',
    loadChildren: () => import('./product/product.module').then(m => m.ProductPageModule)
  },
  {
    path: 'research-filters',
    loadChildren: () => import('./research-filters/research-filters.module').then(m => m.ResearchFiltersPageModule)
  },
  {
    path: 'marketplace-products',
    loadChildren: () => import('./marketplace-products/marketplace-products.module').then(m => m.MarketplaceProductsPageModule)
  },
  {
    path: 'product-detail',
    loadChildren: () => import('./product-detail/product-detail.module').then(m => m.ProductDetailPageModule)
  },
  {
    path: 'panier',
    loadChildren: () => import('./panier/panier.module').then(m => m.PanierPageModule)
  },
  {
    path: 'marketplace-farms',
    loadChildren: () => import('./marketplace-farms/marketplace-farms.module').then(m => m.MarketplaceFarmsPageModule)
  },
  {
    path: 'ajouter-produit-step3',
    loadChildren: () => import('./ajouter-produit-step3/ajouter-produit-step3.module').then(m => m.AjouterProduitStep3PageModule)
  },
  {
    path: 'ajouter-produit-step4',
    loadChildren: () => import('./ajouter-produit-step4/ajouter-produit-step4.module').then(m => m.AjouterProduitStep4PageModule)
  },
  {
    path: 'inscription-consommateur',
    loadChildren: () => import('./inscription-consommateur/inscription-consommateur.module').then(m => m.InscriptionConsommateurPageModule)
  },
  {
    path: 'payment-method',
    loadChildren: () => import('./payment-method/payment-method.module').then(m => m.PaymentMethodPageModule)
  },
  {
    path: 'sms-validation-consommateur',
    loadChildren: () => import('./sms-validation-consommateur/sms-validation-consommateur.module').then(m => m.SmsValidationConsommateurPageModule)
  },
  {
    path: 'email-validation-consommateur',
    loadChildren: () => import('./email-validation-consommateur/email-validation-consommateur.module').then(m => m.EmailValidationConsommateurPageModule)
  },
  {
    path: 'successful-order',
    loadChildren: () => import('./successful-order/successful-order.module').then(m => m.SuccessfulOrderPageModule)
  },
  {
    path: 'order-detail',
    loadChildren: () => import('./order-detail/order-detail.module').then(m => m.OrderDetailPageModule)
  },
  {
    path: 'farm-detail',
    loadChildren: () => import('./farm-detail/farm-detail.module').then(m => m.FarmDetailPageModule)
  },
  {
    path: 'homepage-agriculteur-full',
    loadChildren: () => import('./homepage-agriculteur-full/homepage-agriculteur-full.module').then(m => m.HomepageAgriculteurFullPageModule)
  },
  {
    path: 'myorders',
    loadChildren: () => import('./myorders/myorders.module').then(m => m.MyordersPageModule)
  },
  {
    path: 'profil-agriculteur',
    loadChildren: () => import('./profil-agriculteur/profil-agriculteur.module').then(m => m.ProfilAgriculteurPageModule)
  },
  {
    path: 'profil-consommateur',
    loadChildren: () => import('./profil-consommateur/profil-consommateur.module').then(m => m.ProfilConsommateurPageModule)
  },
  {
    path: 'connexion',
    loadChildren: () => import('./connexion/connexion.module').then(m => m.ConnexionPageModule)
  },
  {
    path: 'connexion-agriculteur',
    loadChildren: () => import('./connexion-agriculteur/connexion.module').then(m => m.ConnexionPageModule)
  },
  {
    path: 'order-detail-consommateur',
    loadChildren: () => import('./order-detail-consommateur/order-detail-consommateur.module').then(m => m.OrderDetailConsommateurPageModule)
  },
  {
    path: 'myorders-consommateur',
    loadChildren: () => import('./myorders-consommateur/myorders-consommateur.module').then(m => m.MyordersConsommateurPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
