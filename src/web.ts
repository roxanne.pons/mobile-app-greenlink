import { WebPlugin } from '@capacitor/core';
import { MyCapacitorPluginPlugin } from './definitions';

export class MyCapacitorPluginWeb extends WebPlugin implements MyCapacitorPluginPlugin {
  constructor() {
    super({
      name: 'MyCapacitorPlugin',
      platforms: ['web'],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }
  async openMap(location: { latitude: number, longitude: number }): Promise<void> {
    alert(location);
  }
}

const MyCapacitorPlugin = new MyCapacitorPluginWeb();

export { MyCapacitorPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(MyCapacitorPlugin);
