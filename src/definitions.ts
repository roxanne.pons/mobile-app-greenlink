declare module '@capacitor/core' {
  interface PluginRegistry {
    MyCapacitorPlugin: MyCapacitorPluginPlugin;
  }
}

export interface MyCapacitorPluginPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
  openMap(location: { latitude: number; longitude: number }): Promise<void>;
}
